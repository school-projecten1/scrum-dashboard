package org.Darma;

import kong.unirest.json.JSONObject;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Sprint {
    private LocalDate startDate,endDate;
    private int jiraId;
    private ArrayList <BurndownChart> burnList = new ArrayList<>();

    public Sprint(LocalDate startDate, LocalDate endDate, int jiraId) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.jiraId = jiraId;
    }

    public Sprint(){

    }

    // Methode hieronder berekent het aantal storypoints dat op done is gezet binnen de startdatum en de datum van nu
    // De storypoints op done haalt hij van het totaal af en wordt in een hashmap gestopt.

    public HashMap sortIssuesByDate(List<Issue> issuelist){
        DateRange dateRange = new DateRange(startDate, LocalDate.now());
        HashMap<LocalDate, Integer> result = new HashMap<>();
        int totalStoryPoints = 45;
        for (LocalDate date : dateRange){
              result.put(date, totalStoryPoints);
            BurndownChart burndownChart = new BurndownChart(date, totalStoryPoints);
            burnList.add(burndownChart);
            for (Issue issue : issuelist){
                if (date.equals(issue.getDatum())){
                    if (issue.getIssueStatus().equalsIgnoreCase("gereed")){
                        totalStoryPoints = totalStoryPoints - issue.getStoryPoints();
                        result.put(date, totalStoryPoints);
                        BurndownChart burndownChart2 = new BurndownChart(date, totalStoryPoints);
                        burnList.add(burndownChart2);

                    }
                }
            }
        }
        return result;
    }

    public Sprint getCurrentprint()
    {
        SendRequest sendRequest = new SendRequest();
        String query = "https://huteam01.atlassian.net/rest/agile/1.0/board/1/sprint?state=active";
        JSONObject resObj = sendRequest.sendRequest(query);
        JSONObject sprintArray = resObj.getJSONArray("values").getJSONObject(0);
        String startDateString = sprintArray.getString("startDate").substring(0, 10);
        String endDateString = sprintArray.getString("endDate").substring(0, 10);
        LocalDate startDate = LocalDate.parse(startDateString);
        LocalDate endDate = LocalDate.parse(endDateString);

        int jiraId = sprintArray.getInt("id");
        return new Sprint(startDate, endDate, jiraId);
    }

    public static int getInfo (String projectName, String taskName )
    {
        SendRequest sendRequest = new SendRequest();
        // get info by project name and task status
        String filter="status=%22"+taskName+"%22%20AND%20project=%22"+projectName+"%22";
        String query="https://huteam01.atlassian.net/rest/api/2/search?jql="+filter;

        JSONObject resObj = sendRequest.sendRequest(query);
        int total = resObj.getInt("total");
        return total;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public ArrayList<BurndownChart> getBurnList() {
        return burnList;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public int getJiraId() {
        return jiraId;
    }

    public void setJiraId(int jiraId) {
        this.jiraId = jiraId;
    }
}
