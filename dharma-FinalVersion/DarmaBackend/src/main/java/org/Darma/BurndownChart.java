package org.Darma;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class BurndownChart extends DataBaseCon{
    private int storyPoints;
    private int totalStoryPoints = 0;
    private List storyList = new ArrayList();
    private LocalDate date;


    public BurndownChart(LocalDate date, int storyPoints){
        this.date = date;
        this.storyPoints = storyPoints;
    }

    public void getStoryPointsFromDB(){
        try {
            // Create our mysql database connection
            Connection myConn = DriverManager.getConnection(getUrl(), getUser(), getPassword());


            // OUR SQL SELECT query
            String query = "SELECT * FROM issues";

            //create the java statement
            Statement st = myConn.createStatement();

            //Execute the query, and get a java resultset
            ResultSet rs = st.executeQuery(query);

            //Iterate through the java resultset
            while (rs.next()){
                storyPoints = rs.getInt("storypoints");
                storyList.add(storyPoints);


                //Print the results
                System.out.println(storyPoints);

            }
            st.close();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Er gaat iets verkeerd");
        }
    }

    public int getTotalStoryPoints(){
        Iterator<Integer> myIterator = storyList.iterator();
             while (myIterator.hasNext()){
                 totalStoryPoints = totalStoryPoints + myIterator.next();
             }
        return totalStoryPoints;
    }

    public List getStoryList(){
        return storyList;
    }
}
