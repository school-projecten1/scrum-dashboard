package org.Darma;


import com.google.gson.Gson;
import spark.Filter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import static spark.Spark.after;
import static spark.Spark.get;

public class JiraClient {
    public static void main(String[] args) throws IOException {
        Sprint sprint= new Sprint().getCurrentprint();

        Issue issue = new Issue();
        ProgressBar progressBar = new ProgressBar();
        HashMap hashMap = sprint.sortIssuesByDate(issue.getAllIssues());

            final Gson gson = new Gson();
            get("/burndown", (req, res) -> {
                return gson.toJson(sprint.getBurnList());
            });

            after((Filter) (request, response) -> {
                response.header("Access-Control-Allow-Origin", "*");
                response.header("Access-Control-Allow-Methods", "GET");
            });


//        get("/progressbar", (req, res) -> {
//            return gson.toJson(progressBar.getProgressDone());
//        });
//
//        after((Filter) (request, response) -> {
//            response.header("Access-Control-Allow-Origin", "*");
//            response.header("Access-Control-Allow-Methods", "GET");
//        });


//        DataBaseCon dataBaseCon = new DataBaseCon();
//        dataBaseCon.addIssueToDataBase();
//        System.out.println("==============================================================================");
//        System.out.println("Startdatum van sprint: "+sprint.getStartDate());
//        System.out.println("EindDatum van Sprint: "+sprint.getEndDate());
//        System.out.println("Id van Sprint: "+sprint.getJiraId());
//        System.out.println("Totaal issues in Sprint Darma :"+ issue.getAllIssues().size());
//        System.out.println("==============================================================================");
//        ProgressData  progressData = new ProgressData();
    }

}
