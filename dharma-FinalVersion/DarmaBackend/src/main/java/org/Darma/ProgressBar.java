package org.Darma;


import java.sql.*;
import java.util.ArrayList;

public class ProgressBar extends DataBaseCon {
    private int storyPoints;
    private double allStoryPoints = 45.0;
    private int totalStoryPoints;

    ArrayList <Double> progressDone = new ArrayList();

    public ProgressBar(){
        try {
            Connection myConn = DriverManager.getConnection(getUrl(), getUser(), getPassword());
            String query = "SELECT storypoints FROM issues WHERE status = 'Gereed'";


            Statement st = myConn.createStatement();
            ResultSet rs = st.executeQuery(query);

            int allStoryPointsDone = 0;
            while(rs.next()){
                allStoryPointsDone = allStoryPointsDone + rs.getInt("storypoints");
            }
            double storyPointsPercentage = allStoryPointsDone / allStoryPoints * 100;
            progressDone.add(storyPointsPercentage);
            //System.out.println(progressDone);

            st.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public ArrayList<Double> getProgressDone() {
        return progressDone;
    }
}

