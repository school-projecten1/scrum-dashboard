package org.Darma;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

public class DataHandler {
    HashMap burnHashmap;
    Issue issue;
    HashMap<LocalDate, Integer> result = new HashMap<>();

    public DataHandler(){
        sortIssuesByDate(issue.getAllIssues());
        getResult();
    }


    public HashMap sortIssuesByDate(List<Issue> issuelist){
        Sprint sprint = new Sprint();
        DateRange dateRange = new DateRange(sprint.getStartDate(), LocalDate.now());
//        HashMap<LocalDate, Integer> result = new HashMap<>();
        int totalStoryPoints = 45;
        for (LocalDate date : dateRange){
            result.put(date, totalStoryPoints);
            for (Issue issue : issuelist){
                if (date.equals(issue.getDatum())){
                    if (issue.getIssueStatus().equalsIgnoreCase("gereed")){
                        totalStoryPoints = totalStoryPoints - issue.getStoryPoints();
                        result.put(date, totalStoryPoints);
                    }
                }
            }
        }
        return result;
    }

    public HashMap getBurnHashmap(){
        return burnHashmap;
    }

    public HashMap getResult(){
        return result;
    }
}
