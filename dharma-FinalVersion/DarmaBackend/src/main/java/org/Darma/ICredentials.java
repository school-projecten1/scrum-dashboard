package org.Darma;
import org.apache.http.HttpRequest;

public interface ICredentials {

    void authenticate(HttpRequest req);

}