package org.Darma;

import com.google.gson.Gson;

import java.time.LocalDate;
import java.util.HashMap;

import static spark.Spark.after;
import static spark.Spark.get;
import static spark.route.HttpMethod.after;
import spark.Filter;

public class EndPoints {
    private DataHandler dataHandler;
    private HashMap hashMap;
    private Sprint sprint;
    private Issue issue;


    public EndPoints (Sprint sprint){
        this.sprint = sprint;
    }

    public EndPoints(){
        startServer();
    }

    public void startServer(){
        Sprint sprint1 = new Sprint();
        final Gson gson = new Gson();
        Issue issue = new Issue();
        HashMap hashMap = sprint1.sortIssuesByDate(issue.getAllIssues());
        get("/burndown", (req, res) -> {
            return gson.toJson(hashMap);
        });

        after((Filter) (request, response) -> {
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Methods", "GET");
        });

    }
}
