import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.IOException;

public class ReadDarmaDataJsonFile {
    public static void main(String[] args) throws IOException, ParseException {

        JSONParser parser = new JSONParser();
        //read data from data.json file
        FileReader fileReader = new FileReader("./DataDarma.json");

        // convert to java object
        Object obj = parser.parse(fileReader);
        //convert to json object
        JSONObject jsonObject = (JSONObject) obj;
        //Convert all issues in java
        int length = jsonObject.size();

        for (int i = 0; i<length; i++) {
            JSONArray jsonArray = (JSONArray) jsonObject.get("issues");
            JSONObject issues= (JSONObject) jsonArray.get(i);
            String expand= (String) issues.get("expand");
            String id = (String) issues.get("id");
            String key= (String) issues.get("key");

    // get data in inner array to string
    String innerFieldsArray =issues.get("fields").toString();


    //System.out.println(expand);
   // System.out.println(id);
    System.out.println(key);
    //System.out.println(innerFieldsArray);

        }
    }
}