async function realData() {
  // const burndownData = await getData();
  // const id = burndownData.map((x) => x.id);
  // const storypoints = burndownData.map((x) => x.storypoints);

  const realBurnDownData = await getRealData();
  const storypoints = realBurnDownData.map((x) => x.storyPoints);
  const date = realBurnDownData.map((x) => x.date["year" + "-" + "month"]);

  let ctx = document.getElementById("burndownChart").getContext("2d");
  let myChart1 = new Chart(ctx, {
    type: "line",
    data: {
      labels: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
      datasets: [
        {
          label: "To do",
          data: storypoints,
          backgroundColor: "transparent",
          borderColor: "red",
          borderWidth: 4,
        },

        {
          label: "Sprint doel",
          data: [45, 40.5, 36, 31.5, 27, 22.5, 18, 13.5, 9, 4.5, 0],
          backgroundColor: "transparent",
          borderColor: "green",
          borderWidth: 4,
        },
      ],
    },
    options: {
      elements: {
        line: {
          tension: 0,
        },
      },
      scales: {
        y: {
          beginAtZero: true,
        },
      },
    },
  });
}

realData();

// async function getData() {
//   const apiURL = "http://localhost:4567/burn";

//   const response = await fetch(apiURL);

//   const burndownData = await response.json();

//   return burndownData;
// }

async function getRealData() {
  const apiURL = "http://localhost:4567/burndown";

  const response = await fetch(apiURL);

  const burndownDataReal = await response.json();

  console.log(burndownDataReal);
  return burndownDataReal;
}

// getRealData();
